# star-wars-project

Used libraries in this project:
- Coroutines
- Retrofit
- LiveData
- Fragment, ViewModel (for working with lifecycle)
- Dagger Hilt (Dependency Injection)
- RecyclerView
- Timber
