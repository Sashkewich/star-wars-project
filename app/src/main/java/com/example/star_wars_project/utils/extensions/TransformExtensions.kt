package com.example.rick_and_morty_mvvm_app.utils.extensions

fun String.removeNulls(): String {
    return if (this.contains(".") && this.substring(this.length - 1) == "0") {
        var count = 0
        for (i in this.length - 1 downTo this.indexOf(".")) {
            if (this.toCharArray()[i] == '0') {
                count++
            }
        }
        this.substring(0, this.length - count)
    } else {
        this
    }
}

fun String.removeNullsWithDot(): String =
    this.replace(",", ".").replace("\\.0*$".toRegex(), "").removeNulls()

fun Double.formatDouble(): String =
    String.format("%.2f", this)

