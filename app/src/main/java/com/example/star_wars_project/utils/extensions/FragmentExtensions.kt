package com.example.star_wars_project.utils.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.star_wars_project.R


fun Fragment.replace(fragment: Fragment) {
    val fragmentManager = requireActivity().supportFragmentManager
    val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
    fragmentTransaction.addToBackStack(null)
        .replace(R.id.container, fragment)
        .commit()
}
