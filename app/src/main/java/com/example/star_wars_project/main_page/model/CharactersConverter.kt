package com.example.star_wars_project.main_page.model

import com.example.star_wars_project.main_page.api.model.CharacterDataResponse
import com.example.star_wars_project.main_page.api.model.CharacterResultResponse

object CharactersConverter {
    fun convert(data: CharacterDataResponse) =
        CharacterData(
            count = data.count,
            next = data.next,
            previous = data.previous,
            results = data.results?.let { convertResults(it) },
        )

    private fun convertResults(results: List<CharacterResultResponse>) =
        results.map {
            CharacterResult(
                gender = it.gender,
                name = it.name,
                starships = it.starships,
            )
        }
}