package com.example.star_wars_project.main_page.model

import com.example.star_wars_project.main_page.api.model.StarshipDataResponse
import com.example.star_wars_project.main_page.api.model.StarshipResultResponse

object StarShipsConverter {
    fun convert(data: StarshipDataResponse) =
        StarshipData(
            count = data.count,
            next = data.next,
            previous = data.previous,
            results = data.results?.let { convertResults(it) },
        )

    private fun convertResults(results: List<StarshipResultResponse>) =
        results.map {
            StarshipResult(
                cargoCapacity = it.cargoCapacity,
                created = it.created,
                edited = it.edited,
                films = it.films,
                name = it.name,
                url = it.url,
                consumables = it.consumables,
                costInCredits = it.costInCredits,
                crew = it.crew,
                hyperdriveRating = it.hyperdriveRating,
                mGLT = it.mGLT,
                manufacturer = it.manufacturer,
                maxAtmospheringSpeed = it.maxAtmospheringSpeed,
                model = it.model,
                passengers = it.passengers,
                pilots = it.pilots,
                starshipClass = it.starshipClass
                )
        }
}