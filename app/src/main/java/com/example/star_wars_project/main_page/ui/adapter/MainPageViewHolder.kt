package com.example.star_wars_project.main_page.ui.adapter

import android.R.id.message
import android.annotation.SuppressLint
import android.app.PendingIntent.getActivity
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.star_wars_project.databinding.ItemCardBinding
import com.example.star_wars_project.main_page.model.CharacterResult
import com.example.star_wars_project.root.RootActivity


class MainPageViewHolder(
    private val binding: ItemCardBinding,
) : RecyclerView.ViewHolder(binding.root) {

    constructor(
        parent: ViewGroup,
    ) : this(
        ItemCardBinding.inflate(LayoutInflater.from(parent.context), parent, false),
    )

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun onBind(item: CharacterResult) {
        with(binding) {
            textName.text = item.name
            genderText.text = "Gender: ${item.gender?.replaceFirstChar(Char::titlecase)}"
            countStarshipsText.text = "Starships controlled: ${item.starships?.size.toString()}"
        }
    }
}