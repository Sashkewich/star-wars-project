package com.example.star_wars_project.main_page.api.model


import com.google.gson.annotations.SerializedName

data class CharacterResultResponse(
    @SerializedName("gender")
    val gender: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("starships")
    val starships: List<String>?,
)