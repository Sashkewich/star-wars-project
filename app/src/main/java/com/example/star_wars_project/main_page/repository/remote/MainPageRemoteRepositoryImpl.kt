package com.example.star_wars_project.main_page.repository.remote

import com.example.star_wars_project.common.di.AppModule
import com.example.star_wars_project.main_page.api.StarWarsApi
import com.example.star_wars_project.common.di.AppModule.Companion.IoDispatcher
import com.example.star_wars_project.main_page.model.CharactersConverter
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainPageRemoteRepositoryImpl @Inject constructor(
    private val api: StarWarsApi,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : MainPageRemoteRepository {
    override suspend fun getCharacters(name: String) =
        withContext(ioDispatcher) {
            CharactersConverter.convert(
                api.getCharacters(name)
            )
        }

//    override suspend fun getStarships(name: String) =
//        withContext(ioDispatcher) {
//            StarShipsConverter.convert(
//                api.getStarships(name)
//            )
//        }
}