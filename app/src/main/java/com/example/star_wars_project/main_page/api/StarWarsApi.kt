package com.example.star_wars_project.main_page.api

import com.example.star_wars_project.main_page.api.model.CharacterDataResponse
import com.example.star_wars_project.main_page.api.model.StarshipDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface StarWarsApi {
    @GET("people?")
    suspend fun getCharacters(
        @Query("search") characterName: String
    ): CharacterDataResponse

    @GET("starship")
    suspend fun getStarships(
        @Query("q") starshipName: String
    ): StarshipDataResponse
}