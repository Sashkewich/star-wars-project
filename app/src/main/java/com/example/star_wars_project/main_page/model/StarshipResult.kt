package com.example.star_wars_project.main_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StarshipResult(
    val cargoCapacity: String?,
    val consumables: String?,
    val costInCredits: String?,
    val created: String?,
    val crew: String?,
    val edited: String?,
    val films: List<String>?,
    val hyperdriveRating: String?,
    val mGLT: String?,
    val manufacturer: String?,
    val maxAtmospheringSpeed: String?,
    val model: String?,
    val name: String?,
    val passengers: String?,
    val pilots: List<String>?,
    val starshipClass: String?,
    val url: String?,
    val favorite: Boolean = false
): Parcelable