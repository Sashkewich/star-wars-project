package com.example.star_wars_project.main_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CharacterResult(
    val gender: String?,
    val name: String?,
    val starships: List<String>?
) : Parcelable