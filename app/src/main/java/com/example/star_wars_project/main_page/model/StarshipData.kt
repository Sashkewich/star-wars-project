package com.example.star_wars_project.main_page.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class StarshipData(
    val count: Int,
    val next: String?,
    val previous: String?,
    val results: List<StarshipResult>?
) : Parcelable