package com.example.star_wars_project.main_page.ui

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rick_and_morty_mvvm_app.utils.extensions.viewbinding.viewBinding
import com.example.star_wars_project.R
import com.example.star_wars_project.common.mvvm.BaseFragment
import com.example.star_wars_project.databinding.FragmentMainPageBinding
import com.example.star_wars_project.main_page.model.CharacterResult
import com.example.star_wars_project.main_page.ui.adapter.MainPageAdapter
import com.example.star_wars_project.main_page.ui.viewmodel.MainPageViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainPageFragment : BaseFragment(R.layout.fragment_main_page) {
    private val binding: FragmentMainPageBinding by viewBinding()
    private val viewModel: MainPageViewModel by viewModels()

    private val adapter = MainPageAdapter()
    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(context)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(viewModel) {
            characterLiveData.observe(viewLifecycleOwner) { characters ->
                if (characters != null) {
                    showCharacters(characters)
                }
            }
        }

        with(binding) {
            recyclerViewStarWars.adapter = adapter
            recyclerViewStarWars.layoutManager = layoutManager
            recyclerViewStarWars.setHasFixedSize(true)

            viewModel.isLoading.observe(viewLifecycleOwner) { isLoading ->
                binding.progress.isVisible = isLoading
            }

            textInput.doAfterTextChanged {
                val name = it.toString()
                if (name.length >= 2) {
                    viewModel.getCharacters(name)
                } else {
                    viewModel.characterLiveData.value = emptyList()
                }
            }
        }
    }

    private fun showCharacters(data: List<CharacterResult>) {
        adapter.setCharacters(data)
    }
}