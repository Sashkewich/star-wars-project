package com.example.star_wars_project.main_page.interactor

import com.example.star_wars_project.common.di.AppModule.Companion.DefaultDispatcher
import com.example.star_wars_project.main_page.repository.remote.MainPageRemoteRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class MainPageInteractor @Inject constructor(
    private val remoteRepository: MainPageRemoteRepository,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
){
    suspend fun getCharacters(name: String) =
        remoteRepository.getCharacters(name)

//    suspend fun getStarships(name: String) =
//        remoteRepository.getCharacters(name)
}