package com.example.star_wars_project.main_page.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.star_wars_project.common.mvvm.BaseViewModel
import com.example.star_wars_project.main_page.interactor.MainPageInteractor
import com.example.star_wars_project.main_page.model.CharacterResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


@HiltViewModel
class MainPageViewModel @Inject constructor(
    private val interactor: MainPageInteractor
) : BaseViewModel() {
    val characterLiveData = MutableLiveData<List<CharacterResult>?>()
    val isLoading = MutableLiveData<Boolean>()

    fun getCharacters(name: String) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val characterData = interactor.getCharacters(name).results
                characterLiveData.value = characterData
                Timber.i("LIVEDATA ---------------> $characterLiveData")
            } catch (t: Throwable) {
                Timber.e(t.message)
            } catch (e: CancellationException) {
                Timber.e(e.message)
            } finally {
                isLoading.value = false
            }

        }
    }

}