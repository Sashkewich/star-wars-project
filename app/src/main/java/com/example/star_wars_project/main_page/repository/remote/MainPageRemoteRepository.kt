package com.example.star_wars_project.main_page.repository.remote

import com.example.star_wars_project.main_page.model.CharacterData
import com.example.star_wars_project.main_page.model.StarshipData

interface MainPageRemoteRepository {
    suspend fun getCharacters(name: String): CharacterData
//    suspend fun getStarships(name: String): StarshipData
}