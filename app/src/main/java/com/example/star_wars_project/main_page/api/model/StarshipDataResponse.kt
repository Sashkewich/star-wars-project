package com.example.star_wars_project.main_page.api.model


import com.google.gson.annotations.SerializedName

data class StarshipDataResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("next")
    val next: String?,
    @SerializedName("previous")
    val previous: String?,
    @SerializedName("results")
    val results: List<StarshipResultResponse>?
)