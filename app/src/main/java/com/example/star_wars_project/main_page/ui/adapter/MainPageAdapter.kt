package com.example.star_wars_project.main_page.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.star_wars_project.R
import com.example.star_wars_project.main_page.model.CharacterResult

class MainPageAdapter() : RecyclerView.Adapter<MainPageViewHolder>() {

    val data = mutableListOf<CharacterResult>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : MainPageViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_card, parent, false)
        return MainPageViewHolder(parent)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MainPageViewHolder, position: Int) {
        val listItem = data[position]
        holder.onBind(listItem)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setCharacters(items: List<CharacterResult>){
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }
}
