package com.example.star_wars_project.common.di

import com.example.star_wars_project.main_page.api.StarWarsApi
import com.example.star_wars_project.main_page.interactor.MainPageInteractor
import com.example.star_wars_project.main_page.repository.remote.MainPageRemoteRepository
import com.example.star_wars_project.main_page.repository.remote.MainPageRemoteRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {
    @Binds
    abstract fun bindMainPageRemoteRepository(
        remoteRepository: MainPageRemoteRepositoryImpl
    ): MainPageRemoteRepository


    companion object {
        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): StarWarsApi =
            retrofit.create(StarWarsApi::class.java)

        @Provides
        @Singleton
        fun provideMainPageInteractor(
            remoteRepository: MainPageRemoteRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): MainPageInteractor =
            MainPageInteractor(remoteRepository, defaultDispatcher)



        // Dispatchers Injection
        @IoDispatcher
        @Provides
        fun provideContextIo(): CoroutineDispatcher = Dispatchers.IO

        @DefaultDispatcher
        @Provides
        fun provideContextDefault(): CoroutineDispatcher = Dispatchers.Default

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class IoDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class DefaultDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class MainDispatcher

        @Retention(AnnotationRetention.BINARY)
        @Qualifier
        annotation class MainImmediateDispatcher
    }
}