package com.example.star_wars_project.root

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.star_wars_project.R
import com.example.star_wars_project.databinding.ActivityRootBinding
import com.example.star_wars_project.favorite_page.ui.FavoriteFragment
import com.example.star_wars_project.main_page.ui.MainPageFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RootActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRootBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        binding = ActivityRootBinding.inflate(layoutInflater)
        setContentView(binding.root)
        replace(MainPageFragment())

        binding.bottomNavigation.setOnNavigationItemReselectedListener { item ->
            when(item.itemId) {
                R.id.home_page -> {
                    replace(MainPageFragment())

                }
                R.id.favorite_page -> {
                    replace(FavoriteFragment())
                }
            }
        }
    }

    private fun replace(fragment: Fragment, tag: String = fragment::class.java.name) {
        val fragmentManager = this.supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction
            .replace(R.id.container, fragment, tag)
            .addToBackStack(tag)
            .commit()
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Are you sure to exit from app?")
        builder.setCancelable(true)
        builder.setNegativeButton("No") { dialog, _ ->
            dialog.cancel()
        }

        builder.setPositiveButton("Yes") { _, _ ->
            finish()
        }

        val dialogAlert = builder.create()
        dialogAlert.show()
    }
}